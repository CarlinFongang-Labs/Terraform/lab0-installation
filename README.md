# Terrarm | Création d'un compte AWS & installation du plugin Terraform pour VSCode

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
1. Créer un compte AWS
2. Protéger le compte root avec un mot de passe fort
3. Créer un compte nominatif avec les droits d'administration complète permettant de déployer les ressources
4. Installer un plugin syntaxique pour faciliter la correction de la syntaxe Terraform

### 1. Création d'un compte AWS
Pour créer un compte Amazon Web Services (AWS), il est nécessaire de suivre une série d'étapes permettant d'accéder à une vaste gamme de services cloud. Voici un guide détaillé pour la création d'un compte AWS :

1. Accéder au site Web d'AWS
   - Accéder au site officiel d'AWS : [https://aws.amazon.com](https://aws.amazon.com).
   - Sélectionner le bouton **"Créer un compte AWS"** ou **"Sign In to the Console"** si des identifiants Amazon.com existent déjà. Les mêmes identifiants peuvent être utilisés.
   >![alt text](img/image-1.png)

2. Démarrer le processus d'inscription
   - Entrer une **adresse e-mail**, renseigner le **Nom du compte AWS**, puis cliquer sur **"Continuer"** pour initier le processus d'inscription.
   >![alt text](img/image-2.png)
   - Un code de vérification est envoyé dans la boite mail, l'entrer dans le champ **"Code de vérification"** puis cliquer sur **"Continuer"**.
   - Remplir le formulaire d'inscription avec un nom, une adresse e-mail et un mot de passe sécurisé.
   >![alt text](img/image-3.png)

3. Compléter avec les coordonnées
   - Le formulaire suivant demande de fournir des informations personnelles ou des informations relatives à une entreprise.
   - Un numéro de téléphone est également requis pour la vérification du compte.
   >![alt text](img/image-4.png)

4. Entrer les informations de paiement
   - Les détails d'une carte de crédit doivent être saisis. AWS requiert ces informations pour couvrir les frais éventuels liés à l'utilisation de services payants.
   - Une autorisation préalable sur la carte est demandée pour vérification, généralement un montant minime remboursé par la suite.
   >![alt text](img/image-5.png)

5. Sélectionner un plan de support
   - Un plan de support doit être choisi. Le **plan de support "Basic"** est gratuit pour les nouveaux utilisateurs et offre un accès à la documentation, aux forums de support et au support pour les questions de compte.

6. Se connecter à la console de gestion AWS
   - Une fois l'inscription terminée, se connecter à la [console de gestion AWS](https://console.aws.amazon.com/) avec l'adresse e-mail et le mot de passe créés.
   - Cette console donne accès à tous les services AWS, permettant de les explorer et de les utiliser.
   >![alt text](img/image-6.png)

7. Explorer l'offre gratuite d'AWS
   - AWS propose un niveau gratuit pour les nouveaux comptes, offrant la possibilité d'utiliser certains services AWS gratuitement, sous certaines limites, pendant les 12 premiers mois.
   - Les détails de l'offre gratuite sont disponibles sur [https://aws.amazon.com/free/](https://aws.amazon.com/free/), et il est recommandé de les consulter pour comprendre ce qui est inclus.
   >![alt text](img/image-7.png)

Il est conseillé de surveiller l'utilisation des services pour éviter des frais inattendus, en particulier lors de l'expérimentation avec des services payants. AWS fournit des outils de gestion des coûts et des alertes de facturation pour aider à gérer la consommation.

### 2. Installation du plugin Terraform pour VSCode
- Ce plugin permet de faciliter la création de projets Terraform et la correction des syntaxes.
  >![alt text](img/image.png)
